var obj = {
    className: 'open menu'
}

// addClass(obj, 'new');   // obj.className='open menu new'
// addClass(obj, 'open');  // без змін (клас вже існує)
// addClass(obj, 'me');
// alert( obj.className );
//
// alert(camelize("background-color")); //== 'backgroundColor';
// alert(camelize("list-style-image")); //== 'listStyleImage';
// alert(camelize("-webkit-transition")); //== 'WebkitTransition';
/*
obj = {
    className: 'open menu menu zero'
};

// removeClass(obj, 'open');   // obj.className='menu'
removeClass(obj, 'menu'); // без змін (такого класу немає)
console.log(obj.className);

var arr = [5, 3, 8, 1];

filterRangeInPlace(arr, 1, 4);

alert( arr ); // заливаються [3, 1]
*/
arr = [5, 2, 1, -10, 8];

reverseSort( arr );
alert( arr ) // 8, 5, 2, 1, -10
/*
arr = ["HTML", "JavaScript", "CSS"];

arrSorted = arraySort( arr );

alert( arrSorted ); // CSS, HTML, JavaScript
alert( arr );       // HTML, JavaScript, CSS

arr = [1, 2, 3, 4, 5];

arr.sort(randomSort);

alert( arr ); // елементи у випадковому порядку, наприклад [3,5,1,2,4]

var vasya = { name: "Вася", age: 23 };
var masha = { name: "Маша", age: 18 };
var vovochka = { name: "Вовочка", age: 6 };

var people = [ vasya , masha , vovochka ];
sortByAge(people);

alert(people[0].age);
alert(people[1].age);

var list = {
    value: 1,
    next: {
        value: 2,
        next: {
            value: 3,
            next: {
                value: 4,
                next: null
            }
        }
    }
};

printList(list);
printListRec(list);
printReverseListRec(list);
printReverseList(list);

var strings = ["кришна", "кришна", "харе", "харе",
    "харе", "харе", "кришна", "кришна", "8-()"
];

alert( unique(strings) ); // C++, C#, C, JavaScript
