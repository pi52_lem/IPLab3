function addClass(obj, newClass) {
    var r = new RegExp("(^|\\s)" + newClass + "($|\\s)", 'g');
    if (obj.className.search(r) == -1) {
        obj.className += ' ' + newClass;
    }
}

function camelize(str) {
    for (var i = 1; i < str.length; i++) {
        if (str.charAt(i - 1) == '-') {
            var tmp = str.charAt(i).toUpperCase();
            str = str.slice(0, i - 1) + tmp + str.slice(i + 1);
            i = 0;
            if (str.indexOf('-') == -1)
                return str;
        }
    }
    return str;
}

function removeClass(obj, removedClassName) {
    var ind;
    while ((ind = obj.className.indexOf(removedClassName)) != -1) {
        var tmp = obj.className.slice(0, ind);
        if(tmp == ' ') {
            obj.className = obj.className.slice(1);
            ind--;
        }
        obj.className = obj.className.slice(1);
        var classes = obj.className.slice(ind);
        var i = classes.indexOf(' ');
        if (i != -1) {
            var tmp2 = classes.slice(i);
        } else
            tmp2 = '';
        obj.className = tmp + tmp2;
    }
    if (obj.className[0] == ' ') {
        obj.className = obj.className.slice(1);
    }
    if (obj.className[obj.className.length - 1] == ' ') {
        obj.className = obj.className.slice(0, -1);
    }
    obj.className = obj.className.replace('  ', ' ');
    while ((ind = obj.className.indexOf('  ')) != -1) {
        var tmp = obj.className.slice(0, ind);
        var tmp2 = obj.className.slice(ind + 1);
        obj.className = tmp + tmp2;
    }
}
function filterRangeInPlace(arr, a, b) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] < a || arr[i] > b) {
            arr.splice(i, 1);
            i--;
        }
    }
}

function reverseSort(arr) {
    arr.sort(function (a, b) {
        if (a < b)
            return 1;
        else return 0;
    });
}

function arraySort(arr) {
    var res = [];
    for (var i = 0; i < arr.length; i++) {
        res[i] = arr[i];
    }
    res.sort(function (a, b) {
        if (a > b)
            return 1;
        else return 0;
    });
    return res;
}

function randomSort(a, b) {
    var r = Math.round(Math.random());
    if (r == 1)
        return 1;
    else return 0;
}

function sortByAge(arr) {
    arr.sort(function (a, b) {
        if (a.age > b.age)
            return 1;
        return 0;
    });
}

function find(array, value) {

    for (var i = 0; i < array.length; i++) {
        if (array[i] == value) return i;
    }

    return -1;
}

function unique(arr) {
    var res = [];
    var g = 0, k;
    var a2 = [];
    for (var i = 0; i < arr.length; i++) {
        a2[i] = arr[i];
    }
    while(k = a2.shift()){
        if(find(res, k) == -1) {
            res[g] = k;
            g++;
        }
    }
    return res;
}

    function printList(list) {
        var res = [];
        var i = 0;
        while(list) {
            res[i] = list.value;
            i++;
            list = list.next;
        }
        alert(res);
    }

function printListRec(list) {
    console.log(list.value);
    if (list.next) {
        printListRec(list.next);
    }
}
function printReverseListRec(list) {
    if (list.next) {
        printReverseListRec(list.next);
    }
    console.log(list.value);
}
function printReverseList(list) {
    var arr = [];
    var tmp = list;
    while (tmp) {
        arr.push(tmp.value);
        tmp = tmp.next;
    }
    for (var i = arr.length - 1; i >= 0; i--) {
        console.log(arr[i]);
    }
}